﻿using System;

namespace TodoListConsole.Model
{
    public class Note
    {
        private const string WASNOTSET = "не указано";

        private string patronymic;
        private string organisation;
        private string job;
        private string other;

        public string SecondName { get; set; }
        public string FirstName { get; set; }
        public string Patronymic {
            get => patronymic; 
            set
            {
                patronymic = String.IsNullOrWhiteSpace(value) ? WASNOTSET : value; 
            }
        }

        public string PhoneNumber { get; set; }
        public string Country { get; set; }
        public Nullable<DateTime> Birthday { get; set; }

        public string Organisation
        {
            get => organisation;
            set
            {
                organisation = String.IsNullOrWhiteSpace(value) ? WASNOTSET : value;
            }
        }

        public string Job
        {
            get => job;
            set
            {
                job = String.IsNullOrWhiteSpace(value) ? WASNOTSET : value;
            }
        }

        public string Other
        {
            get => other;
            set
            {
                other = String.IsNullOrWhiteSpace(value) ? WASNOTSET : value;
            }
        }

        public override string ToString() => $"Фамилия -\t\t{SecondName}\n" +
                                             $"Имя -\t\t\t{FirstName}\n" +
                                             $"Отчество -\t\t{patronymic}\n" +
                                             $"Телефонный номер -\t{PhoneNumber}\n" +
                                             $"Страна -\t\t{Country}\n" +
                                             $"Дата рождения -\t\t{GetBirthdayFild()}\n" +
                                             $"Организация -\t\t{organisation}\n" +
                                             $"Должность \t\t{job}\n" +
                                             $"Прочее -\t\t{other}";

        public string GetShortInfo() => $"Фамилия - {SecondName}\tИмя - {FirstName}\t Телефонный номер - {PhoneNumber}";

        private string GetBirthdayFild() => Birthday == null ? WASNOTSET : Birthday.Value.ToShortDateString();
    }
}
