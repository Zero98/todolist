﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TodoListConsole.Model
{
    static public class NoteManager
    {
        public static string ParamsList
            { 
                get => "0 - Фамилия\n" +
                "1 - Имя\n" +
                "2 - Отчество\n" +
                "3 - Номер телефона\n" +
                "4 - Страна\n" +
                "5 - День рождения\n" +
                "6 - Организация\n" +
                "7 - Должность\n" +
                "8 - Дополнительная информация";
            }
        private static Dictionary<int, Note> notes = new Dictionary<int, Note>();
        private static int nextId;

        private static Dictionary<int, Action<int, string>> paramsToUpdate = new Dictionary<int, Action<int, string>>
        {
            { 0, UpdateSecondNameById },
            { 1, UpdateFirstNameById },
            { 2, UpdatePatronymicById },
            { 3, UpdatePhoneNumberById },
            { 4, UpdateCountryById},
            { 5, UpdateBirthdayById },
            { 6, UpdateOrganisationById },
            { 7, UpdateJobById },
            { 8, UpdateOtherById },
        };

        public static void AddNewNote(Note note)
        {
            notes.Add(nextId, note);
            nextId++;
        }

        public static bool DelNoteById(int id) => notes.Remove(id);

        public static Note GetNoteById(int id) => notes.ContainsKey(id) ? notes[id] : null;

        public static string GetShortInfoAboutNotes()
        {
            StringBuilder stringBuilder = new StringBuilder();
            foreach (var pair in notes)
            {
                stringBuilder.Append($"{pair.Key}. {pair.Value.GetShortInfo()}\n");
            }
            return stringBuilder.ToString().TrimEnd('\n');
        }

        public static bool UpdateNote(int param,string value, int id)
        {
            if (paramsToUpdate.ContainsKey(param))
            {
                try
                {
                    paramsToUpdate[param](id, value);
                    return true;
                }
                catch
                {
                    return false;
                }
            }
            return false;
        }

        private static void UpdateSecondNameById(int id, string newVal) => notes[id].SecondName = newVal;
        private static void UpdateFirstNameById(int id, string newVal) => notes[id].FirstName = newVal;
        private static void UpdateJobById(int id, string newVal) => notes[id].Job = newVal;
        private static void UpdateOrganisationById(int id, string newVal) => notes[id].Organisation = newVal;
        private static void UpdateOtherById(int id, string newVal) => notes[id].Other = newVal;
        private static void UpdatePatronymicById(int id, string newVal) => notes[id].Patronymic = newVal;
        private static void UpdatePhoneNumberById(int id, string newVal)
        {
            var value = newVal.Replace('-', '\0');
            int.Parse(value);
            if(value.Length < 5 || value.Length > 12)
            {
                throw new Exception();
            }
            notes[id].PhoneNumber = value;
        }
        private static void UpdateCountryById(int id, string newVal) => notes[id].Country = newVal;
        private static void UpdateBirthdayById(int id, string newVal) => notes[id].Birthday = DateTime.Parse(newVal);
    }
}
