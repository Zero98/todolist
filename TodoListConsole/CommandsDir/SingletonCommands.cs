﻿using System.Collections.Generic;

namespace TodoListConsole.CommandsDir
{
    public sealed class SingletonCommands
    {
        private static volatile SingletonCommands instance;
        private static volatile Dictionary<string, AbstractCommands> source;

        private static readonly object syncObj = new object();

        SingletonCommands()
        {
            source = new Dictionary<string, AbstractCommands>
            {
                {"add", new AddCommand() },
                {"del", new DeleteNoteCommand() },
                {"upd", new UpdateNoteCommand() },
                {"showid", new ShowNoteByIdCommand() },
                {"showall", new ShowAllNoteCommand() },
                { "help", new HelpCommand() },
                {"clr", new ClearDisplayCommand() },
                { "exit", new ExitCommand() },
            };
        }

        public static Dictionary<string, AbstractCommands> Source
        {
            get
            {
                if (instance == null)
                {
                    lock (syncObj)
                    {
                        if (instance == null)
                            instance = new SingletonCommands();
                    }
                }

                return source;
            }
        }
    }
}
