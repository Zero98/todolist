﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoListConsole.CommandsDir
{
    public class HelpCommand : AbstractCommands
    {
        private string infoAboutAllCommands;

        public HelpCommand()
        {
            ShortInfo = "Отображение информации обо всех командах";
            Name = "help";
        }

        public override void Execute()
        {
            Menu.DisplayValue(ShortInfo);
            infoAboutAllCommands = InitInfo();
            Menu.DisplayValue(infoAboutAllCommands);
        }

        private string InitInfo()
        {
            var stringBuilder = new StringBuilder();
            foreach (var command in SingletonCommands.Source.Values)
            {
                stringBuilder.Append(command.Name);
                stringBuilder.Append(" - ");
                stringBuilder.Append(command.ShortInfo);
                stringBuilder.Append("\n");
            }

            return stringBuilder.ToString().TrimEnd('\n');
        }
    }
}
