﻿using TodoListConsole.Model;

namespace TodoListConsole.CommandsDir
{
    public class ShowAllNoteCommand : AbstractCommands
    {
        public ShowAllNoteCommand()
        {
            Name = "showall";
            ShortInfo = "Краткая информация о всех записях";
        }

        public override void Execute()
        {
            Menu.DisplayValue(ShortInfo);
            var shortInfo = NoteManager.GetShortInfoAboutNotes();
            if (string.IsNullOrEmpty(shortInfo))
            {
                Menu.DisplayValue("Записей нет!"+ERRORMSG);
            }
            else
            {
                Menu.DisplayValue($"{shortInfo}{SUCCESSMSG}");
            }
        }
    }
}
