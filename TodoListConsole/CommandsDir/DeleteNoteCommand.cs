﻿using TodoListConsole.Model;

namespace TodoListConsole.CommandsDir
{
    public class DeleteNoteCommand : AbstractCommands
    {
        public DeleteNoteCommand()
        {
            Name = "del";
            ShortInfo = "Удаление записи по ее номеру из блокнота";
        }

        public override void Execute()
        {
            int chosenIdI;  
            Menu.DisplayValue(ShortInfo);
            Menu.DisplayValue("Введите номер записи, которую вы желаете удалить");
            var chosenIdS = Menu.GetValue();
            if (int.TryParse(chosenIdS, out chosenIdI))
            {
                if (NoteManager.DelNoteById(chosenIdI))
                {
                    Menu.DisplayValue("Запись успешено удалена!"+SUCCESSMSG);
                }
                else
                {
                    Menu.DisplayValue("Записи с таким номер не существует!"+ERRORMSG);
                }
            }
            else
            {
                Menu.DisplayValue("Вы ввели некорректное значение для id!"+ERRORMSG);
            }
        }
    }
}
