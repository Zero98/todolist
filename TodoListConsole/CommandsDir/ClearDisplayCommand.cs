﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoListConsole.CommandsDir
{
    public class ClearDisplayCommand : AbstractCommands
    {
        public ClearDisplayCommand()
        {
            Name = "clr";
            ShortInfo = "Очистка консоли от лишеней информации";
        }

        public override void Execute()
        {
            Menu.ClearDisplay();
            Menu.DisplayValue("Очистка завершена успешно!"+SUCCESSMSG);
        }
    }
}
