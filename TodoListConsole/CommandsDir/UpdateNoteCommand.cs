﻿using System;
using System.Linq;
using TodoListConsole.Model;

namespace TodoListConsole.CommandsDir
{
    class UpdateNoteCommand : AbstractCommands
    {
        private Note updatedNote;
        public UpdateNoteCommand()
        {
            Name = "upd";
            ShortInfo = "Обновление записей по id";
        }

        public override void Execute()
        {
            Menu.DisplayValue(ShortInfo);
            Menu.DisplayValue("Введите id желаемой записи");
            int chosenIdI;

            if (int.TryParse(Menu.GetValue(), out chosenIdI))
            {
                if (NoteManager.GetNoteById(chosenIdI) == null)
                {
                    Menu.DisplayValue("К сожалению заметки с таким id нет!"+ ERRORMSG);
                }
                else
                {
                    Menu.DisplayValue("Введите параметр для изменения:");
                    Menu.DisplayValue(NoteManager.ParamsList);
                    int chosenParamI;
                    if(int.TryParse(Menu.GetValue(), out chosenParamI))
                    {
                        if (NoteManager.UpdateNote(chosenParamI, GetImportantValue(), chosenIdI))
                        {
                            Menu.DisplayValue("Заметка успешно изменена!" + SUCCESSMSG);
                        }
                        else
                        {
                            Menu.DisplayValue("Не удалось обновить значение!" + ERRORMSG);
                        }
                    }
                    else
                    {
                        Menu.DisplayValue("Вы ввели некорректное значение для id!" + ERRORMSG);
                    }
                }
            }
            else
            {
                Menu.DisplayValue("Вы ввели некорректное значение для id!" + ERRORMSG);
            }
        }

        private string GetImportantValue()
        {
            string importantValue;
            do
            {
                Menu.DisplayValue($"Введите новое значение");
                importantValue = Menu.GetValue();
                if (String.IsNullOrWhiteSpace(importantValue))
                {
                    Menu.ClearDisplay();
                    Menu.DisplayValue("Введите значение еще раз");
                }
            } while (String.IsNullOrWhiteSpace(importantValue));

            return importantValue.TrimStart(' ');
        }
    }
}
