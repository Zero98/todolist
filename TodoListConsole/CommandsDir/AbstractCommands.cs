﻿using System;

namespace TodoListConsole.CommandsDir
{
    public abstract class AbstractCommands
    {
        protected const string ERRORMSG = "\nВведите команду еще раз!";
        protected const string SUCCESSMSG = "\nВведите команду для продолжения работы или exit для выхода.";


        public string ShortInfo { get; protected set; }
        public string Name { get; protected set; }

        public abstract void Execute();

        public virtual double Round(double eps, double value)
        {
            int count = eps.ToString().Length - 2;
            return Math.Round(value, count);
        }
    }
}
