﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TodoListConsole.Model;

namespace TodoListConsole.CommandsDir
{
    public class AddCommand : AbstractCommands
    {
        private Note newNote;

        public AddCommand()
        {
            Name = "add";
            ShortInfo = "Добавление новой записи в блокнот";
            newNote = new Note();
        }

        public override void Execute()
        {
            Menu.DisplayValue(ShortInfo);
            newNote.SecondName = GetImportantValue("фамилия", "Вы начали создание новой записи", ValidString);
            newNote.FirstName = GetImportantValue("имя", "Вы продолжаете создание новой записи", ValidString);
            newNote.Patronymic = GetNotImportantValue("отчество", "Вы продолжаете создание новой записи", ValidString);
            newNote.PhoneNumber = GetImportantValue("номер телефона", "Вы продолжаете создание новой записи", ValidPhone);
            newNote.Country = GetImportantValue("страна", "Вы продолжаете создание новой записи", ValidString);
            var birthDay = GetNotImportantValue("дата рождения", "Вы продолжаете создание новой записи", ValidDay);
            newNote.Birthday = birthDay == null ? null : (Nullable<DateTime>)DateTime.Parse(birthDay);
            newNote.Organisation = GetNotImportantValue("организация", "Вы продолжаете создание новой записи", ValidNotImportantString);
            newNote.Job = GetNotImportantValue("должность", "Вы продолжаете создание новой записи", ValidNotImportantString);
            newNote.Other = GetNotImportantValue("прочие заметки", "Вы продолжаете создание новой записи", ValidNotImportantString);
            NoteManager.AddNewNote(newNote);
            Menu.ClearDisplay();
            Menu.DisplayValue("Заметка успешно добавлена!" + SUCCESSMSG);
        }

        private string GetImportantValue(string paramName, string otherText, Func<string, bool> validFunc)
        {
            Menu.ClearDisplay();
            string importantValue;
            bool isNotValid = false;
            Menu.DisplayValue($"{otherText}\n{paramName}:");
            do
            {
                importantValue = Menu.GetValue().TrimStart(' ').TrimEnd(' ');
                Menu.ClearDisplay();
                isNotValid = string.IsNullOrWhiteSpace(importantValue) || !validFunc(importantValue);
                if (isNotValid)
                {
                    Menu.DisplayValue($"{otherText}\nВы ввели некорректное значение параметра {paramName}\nВведите значение еще раз");
                }
            } while (isNotValid);

            return importantValue.TrimStart(' ');
        }

        private string GetNotImportantValue(string paramName, string otherText,  Func<string, bool> validFunc)
        {
            Menu.ClearDisplay();
            Menu.DisplayValue($"{otherText}\n{paramName} (Параметр не является обязательным):");
            string notImportantValue;
            bool isNotValid = false;
            do
            {
                notImportantValue = Menu.GetValue().TrimStart(' ').TrimEnd(' ');

                Menu.ClearDisplay();
                isNotValid = !string.IsNullOrEmpty(notImportantValue) && !validFunc(notImportantValue);
                if (!validFunc(notImportantValue))
                    Menu.DisplayValue($"{otherText}\nВы ввели некорректное значение параметра {paramName}\nВведите значение еще раз или нажмите Enter");
            } while (isNotValid);
            
            return validFunc(notImportantValue) ? notImportantValue : null;
        }

        private bool ValidNotImportantString(string validParam) => true;

        private bool ValidString(string validParam) => validParam.All(c => Char.IsLetter(c));            

        private bool ValidPhone(string validParam)
        {
            long value;
            var stringWithout = validParam.Trim('-');
            
            return long.TryParse(stringWithout, out value)
                && stringWithout.Length > 5 
                && stringWithout.Length < 12;
        }

        private bool ValidDay(string validParam)
        {
            DateTime dateTime;
            return DateTime.TryParse(validParam, out dateTime);
        }
    }
}
