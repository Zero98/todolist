﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TodoListConsole.CommandsDir
{
    public class ExitCommand : AbstractCommands
    {
        public ExitCommand()
        {
            ShortInfo = "Завершение работы программы";
            Name = "exit";
        }

        public override void Execute() { }
    }
}
