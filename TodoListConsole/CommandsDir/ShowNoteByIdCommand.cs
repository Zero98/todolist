﻿using TodoListConsole.Model;

namespace TodoListConsole.CommandsDir
{

    public class ShowNoteByIdCommand : AbstractCommands
    {
        Note chosenNote;

        public ShowNoteByIdCommand()
        {
            Name = "showid";
            ShortInfo = "Вывод информации о заметке по id";    
        }

        public override void Execute()
        {
            int chosenIdI;
            Menu.DisplayValue(ShortInfo);
            Menu.DisplayValue("Введите id желаемой записи");
            var chosenIdS = Menu.GetValue();
            if(int.TryParse(chosenIdS,out chosenIdI))
            {
                chosenNote = NoteManager.GetNoteById(chosenIdI);
                if (chosenNote == null)
                {
                    Menu.DisplayValue("К сожалению заметки с таким id нет!"+ERRORMSG);
                }
                else
                {
                    Menu.DisplayValue($"{chosenNote.ToString()}{SUCCESSMSG}");
                }
            }
            else
            {
                Menu.DisplayValue("Вы ввели некорректное значение для id!"+ERRORMSG);
            }
        }
    }
}
