﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using System.Threading;
using TodoListConsole.CommandsDir;

namespace TodoListConsole
{
    public class Menu
    {
        private bool exitFlag;

        public Menu() { }

        public void Run()
        {
            DisplayValue("Для начала работы добавьте новую заметку командой add.\n" +
                          "Список всех доступных команд можно получить вызвав команду help.");
            do
            {
                var consoleCommand = GetValue().Trim(' ');

                if (IsValid(consoleCommand))
                {
                    ClearDisplay();
                    SingletonCommands.Source[consoleCommand.ToLower()].Execute();
                }
                else
                {
                    ClearDisplay();
                    DisplayValue("Вы ввели невалидную команду!\nПопробуйте еще раз!");
                    SingletonCommands.Source["help"].Execute();
                }
            } while (!exitFlag);
        }

        public static void Load(TaskAwaiter taskAwaiter)
        {
            var leftPos = Console.CursorLeft;
            var topPos = Console.CursorTop;

            do
            {
                for (int i = 0; i < 3; i++)
                {
                    Console.Write(".");
                    Thread.Sleep(100);
                }
                ClearFromTo(leftPos, topPos, 3);

            } while (!taskAwaiter.IsCompleted);
        }

        private static void ClearFromTo(int leftPos, int topPos, int clearingCount)
        {
            Console.SetCursorPosition(leftPos, topPos);
            for (int i = 0; i < clearingCount; i++)
                Console.Write(" ");
            Console.SetCursorPosition(leftPos, topPos);
        }

        public static string GetValue()
        {
            Console.Write(">");
            var value = Console.ReadLine();
            return value;
        }

        public static void DisplayValue(string value)
        {
            Console.Write($"{value}\n");
        }

        public static void ClearDisplay()
        {
            Console.Clear();
        }

        public static void WaitingExecution(IAsyncResult asyncResult)
        {
            do
            {
                for (int i = 0; i < 3; i++)
                    DisplayValue(".");
                ClearDisplay();
            } while (asyncResult.IsCompleted);
        }

        private bool IsValid(string consoleCommand)
        {
            foreach (var command in SingletonCommands.Source.Keys)
            {
                if (command.Equals(consoleCommand.ToLower()))
                {
                    if (consoleCommand.ToLower().Equals("exit"))
                        exitFlag = true;
                    return true;
                }
            }
            return false;
        }
    }
}