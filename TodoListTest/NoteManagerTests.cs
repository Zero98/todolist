using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using TodoListConsole.Model;

namespace TodoListTest
{
    [TestClass]
    public class NoteManagerTests
    {
        Note testNote = new Note()
        {
            SecondName = "������",
            FirstName = "����",
            Patronymic = "��������",
            Country = "������",
            PhoneNumber = "89606934123",
            Birthday = new DateTime(1997, 12, 24),
            Job = null,
            Organisation = null,
            Other = null
        };

        [TestMethod]
        public void TestAdd()
        {
            NoteManager.AddNewNote(testNote);
            Assert.IsNotNull(NoteManager.GetNoteById(0),"�� ������� ������� ����� ������!");   
        }

        [TestMethod]
        public void TestUpdate()
        {
            var rexpectedStr = "0. ������� - ������\t��� - ����\t ���������� ����� - 89606934123";
            NoteManager.UpdateNote(0, "������", 0);
            var testStr = NoteManager.GetShortInfoAboutNotes();
            Assert.AreEqual<string>(rexpectedStr, testStr, "�� ������� �������� ��������");
        }

        [TestMethod]
        public void TestDel()
        {
            NoteManager.DelNoteById(0);
            Assert.IsNull(NoteManager.GetNoteById(0), "�� ������� ������� ������!");
        }

        [TestMethod]
        public void TestShowAll()
        {
            var expected = "";
            var testStr = NoteManager.GetShortInfoAboutNotes();
            Assert.AreEqual(expected, testStr, "��������� ������");
        }
    }
}
